package cn.lyc.controller;


import cn.lyc.api.service.SysUserService;
import cn.lyc.entity.SysUser;
import cn.lyc.logs.LogsAnnotation;
import cn.lyc.utils.R;
import cn.lyc.utils.ShiroUtil;
import cn.lyc.utils.TableReturn;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 用户功能控制器
 */
@RestController
@Slf4j
public class SysUserController {
	
	@Autowired
	private SysUserService sysUserService;
	
	
    /**
     * 用户表格数据
     * @param map
     * @return
     */
    @RequestMapping("sys/user/list")
    @RequiresPermissions("sys:user:list")
    public TableReturn list(@RequestBody Map<String,Object> map){
//        List<SysUser> userList = sysUserService.queryList(map);
//        return new TableReturn(userList,sysUserService.queryListCount(map));
        SysUser sysUser = ShiroUtil.getUserEntity();
        if("" == map.get("company_id")){
            map.put("company_id",sysUser.getCompany_id());
        }
        map.put("provider_id",sysUser.getProvider_id());
//        List<SysUser> userList = sysUserService.queryList(map);
        List<SysUser> userList = sysUserService.queryUsers(map);
        int count = sysUserService.queryUsersCount(map);
        return new TableReturn(userList,count);
    }
    
    
    /**
     * 更新一个用户
     * @param id
     * @return
     */
    @RequestMapping("sys/user/delete/{id}")
    @RequiresPermissions("sys:user:delete")
    public R delete(@PathVariable("id")Long id){
        SysUser user = new SysUser();
        user.setId(id);
        user.setIs_delete(0);
        int row = sysUserService.updateUser(user);
        if(row != 1){
            return R.error("删除失败");
        }
        return R.ok();
    }
    
    /**
     * 启用一个用户
     * @param id
     * @return
     */
    @RequestMapping("sys/user/use/{id}")
    public R use(@PathVariable("id")Long id){
        SysUser user = new SysUser();
        user.setId(id);
        user.setIs_delete(1);
        int row = sysUserService.updateUser(user);
        if(row != 1){
            return R.error("启用失败");
        }
        return R.ok();
    }


    /**
     * 保存用户信息
     * @param user
     * @return
     */
    @RequestMapping("sys/user/addUser")
    @RequiresPermissions("sys:user:save")
    @ResponseBody
    @LogsAnnotation(detail = "添加用户")
    public R addUser(@RequestBody SysUser user){
        SysUser sysUser = ShiroUtil.getUserEntity();
        user.setProvider_id(sysUser.getProvider_id());
        user.setCompany_id(sysUser.getCompany_id());
        user.setCreate_user(ShiroUtil.getUserEntity().getId());
        if(user.getProvider_id() == null){
            user.setType(1);
        }else if(user.getCompany_id() == null){
            user.setType(2);
        }else{
            user.setType(3);
        }
        sysUserService.addUser(user,1);
        return R.ok();
    }


    /**
     * 更新一个用户
     * @param user
     * @return
     */
    @RequestMapping("sys/user/updateUser")
    @RequiresPermissions("sys:user:update")
    public R updateUser(@RequestBody SysUser user){
        int row = sysUserService.updateUser(user);
        if(row != 1){
            return R.error("更新失败");
        }
        return R.ok();
    }

    @RequestMapping("sys/user/nameList")
    @RequiresPermissions("sys:user:list")
    public TableReturn queryNameList(@RequestBody Map<String,Object> map){
//        List<SysUser> userList = sysUserService.queryNameList(map);
        List<SysUser> userList = sysUserService.queryList(map);
//        int count = sysUserService.queryNameListCount(map);;
        int count = sysUserService.queryListCount(map);
        return new TableReturn(userList,count);
    }

    @RequestMapping("sys/user/roleList")
    @RequiresPermissions("sys:user:list")
    public TableReturn queryRoleList(@RequestBody Map<String,Object> map){
        List<SysUser> userList = sysUserService.queryRoleList(map);
        int count = sysUserService.queryRoleListCount(map);
        return new TableReturn(userList,count);
    }
}
