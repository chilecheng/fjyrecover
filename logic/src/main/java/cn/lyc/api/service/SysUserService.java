package cn.lyc.api.service;

import cn.lyc.entity.SysUser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;


public interface SysUserService {



	
	//根据用户名查询
	public SysUser query_userName(@RequestParam("username") String username);

	//防止删除数据库中admin
	void addAdmin(@RequestBody SysUser users);
	
    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(@RequestParam("userId") Long userId);
	
	List<SysUser> queryUsers(Map<String,Object> map);
	
	int queryUsersCount(Map<String,Object> map);
	
    /**
     * 更新账号
     * @param user
     * @return
     */
    int updateUser(@RequestBody SysUser user);

	/**
	 * 保存
	 * @param user
	 * @param type 0表示接入企业或服务商时同时创建的用户 ，用户角色根据条件自动生成
	 *             1表示账户管理中添加生成的用户，创建时选择用户角色
	 */
	void addUser(@RequestBody SysUser user,int type);

	List<SysUser> queryList(Map<String,Object> map);
	int queryListCount(Map<String,Object> map);

	List<SysUser> queryRoleList(Map<String,Object> map);
	int queryRoleListCount(Map<String,Object> map);
}
