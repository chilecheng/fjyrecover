package cn.lyc.utils;
import lombok.Data;

@Data
public class TableReturn {

	private Integer code;
	private Object rows;
	private int total;
	public TableReturn() {
	}
	public TableReturn(Object rows, int total){
		this.code=0;
		this.rows=rows;
		this.total=total;
	}
}
