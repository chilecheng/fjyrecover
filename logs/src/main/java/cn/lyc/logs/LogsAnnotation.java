package cn.lyc.logs;

import java.lang.annotation.*;

/**
 * @ClassName LogsAnnotation
 * @Description 自定义aop注解
 * @Author wh
 * #Date 2019-09-04 15:02
 */

@Target(value = {ElementType.TYPE, ElementType.METHOD})//使用位置（类，方法）
@Retention(RetentionPolicy.RUNTIME)//加载到jvm里运行
@Documented//说明该注解将被包含在javadoc中
public @interface LogsAnnotation {

    String detail() default "";

}


