package cn.lyc.config.shiro;


import cn.lyc.api.service.SysMenuService;
import cn.lyc.api.service.SysUserService;
import cn.lyc.entity.SysMenu;
import cn.lyc.entity.SysUser;
import cn.lyc.utils.JWTUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MyRealm extends AuthorizingRealm {

    private static final Logger LOGGER = LogManager.getLogger(MyRealm.class);

//    @Autowired
//    private userService userService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private JWTUtil jwtUtil;


    /**
     * 大坑！，必须重写此方法，不然Shiro会报错
     */

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        System.out.println("JWT进行权限验证");
//        String username = JWTUtil.getUsername(principals.toString());
//        SysUser user = userService.query_userName(username);


        /**
         * TODO 进行权限添加
         */



//        System.out.println("————权限认证————");
        //获取登录用户
        SysUser user = (SysUser) principals.getPrimaryPrincipal();
        List<String> permsList;
        Long userId = user.getId();
        if(userId == 1){
            //超级管理员，有最高权限
            List<SysMenu> menuList = sysMenuService.queryList(new HashMap<String, Object>());
            permsList = new ArrayList<>(menuList.size());
            for(SysMenu menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            //非超级管理员，有部分权限
            permsList = userService.queryAllPerms(userId);
        }
        //在权限列表中有很多空
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        System.out.println("JWT身份验证(改写 Shiro 身份验证)");

        String jwtToken = (String) auth.getCredentials();

        String username = null;
        try {

            username =jwtUtil.getUserName(jwtToken);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (username == null) {
            throw new AuthenticationException("请登录");
        }

//        SysUser user = userService.query_userName(username);

        SysUser userBean = userService.query_userName(username);
        if (userBean == null) {
            throw new AuthenticationException("没有找到该用户");
        }

        if (! jwtUtil.verify(jwtToken)) {
            throw new AuthenticationException("登录过期,请重新登录");
        }
        return new SimpleAuthenticationInfo(userBean, jwtToken, "my_realm");

    }
}
