package cn.lyc.api.service;


import cn.lyc.entity.SysMenu;
import cn.lyc.vo.MenuVO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

//@RequestMapping(name = "user")
public interface SysMenuService {
	
	/**
     * 获取用户菜单列表
     */
    List<SysMenu> getUserMenuList(@RequestParam("userId") Long userId);

	List<MenuVO> getUserMenuVOList(@RequestParam("userId") Long userId);
	
	/**
     * 根据父菜单，查询子菜单
     * @param parentId 父菜单ID
     * @param menuIdList  用户菜单ID
     */
    List<SysMenu> queryListParentId(@RequestParam("parentId") Long parentId,@RequestBody List<Long> menuIdList);
	
	/**
     * 查询菜单列表
     */
    List<SysMenu> queryList(Map<String, Object> map);
	
	/**
     * 查询菜单列表（不带分页）总数
     * @return
     */
    int queryTotal();
	
	/**
	 * 添加
	 * @param menu
	 */
	void save(@RequestBody SysMenu menu);
	
	
    /**
     * 查询菜单
     */
    SysMenu queryObject(@RequestParam("menuId") Long menuId);
	
	/**
	 * 修改
	 * @param menu
	 */
	void update(@RequestBody SysMenu menu);
	
	/**
	 * 删除
	 * @param menuIds
	 */
	void deleteBatch(@RequestBody Long[] menuIds);
	
    

}
