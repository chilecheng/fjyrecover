package cn.lyc.controller;

import cn.lyc.api.service.SysLogService;
import cn.lyc.entity.SysLog;
import cn.lyc.utils.R;
import cn.lyc.utils.TableReturn;
import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @ClassName SysLogController
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-23 11:09
 */
@RestController
@Slf4j
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("/sys/log/list")
    public TableReturn queryList(@RequestBody Map<String,Object> map){
        return new TableReturn(sysLogService.queryList(map),sysLogService.queryListTotal(map));
    }
}
