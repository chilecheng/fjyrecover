package cn.lyc.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName SysLog
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-05 15:48
 */
@Data
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long user_id;
    private String method_name;
    private String method_description;
    private String request_ip;
    private Date create_time;

}
