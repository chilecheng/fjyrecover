package cn.lyc.utils;

import cn.lyc.entity.SysUser;
import org.apache.shiro.SecurityUtils;

public class ShiroUtil {
    public static SysUser getUserEntity() {
        return (SysUser)SecurityUtils.getSubject().getPrincipal();
    }
}
