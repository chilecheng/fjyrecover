package cn.lyc.api.service;

import cn.lyc.entity.SysLog;

import java.util.List;
import java.util.Map;

/**
 * @ClassName SysLogService
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-23 11:05
 */
public interface SysLogService {

    /**
     * 按条件查询操作
     *
     * @param map
     * @return
     */
    List<SysLog> queryList(Map<String, Object> map);

    /**
     * 添加操作日志记录
     *
     * @param sysLog
     */
    void save(SysLog sysLog);

    /**
     * 日志记录数
     * @param map
     * @return
     */
    int queryListTotal(Map<String,Object> map);
}
