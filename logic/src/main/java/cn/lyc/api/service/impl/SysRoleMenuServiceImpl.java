package cn.lyc.api.service.impl;


import cn.lyc.api.service.SysRoleMenuService;
import cn.lyc.dao.SysRoleMenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
@CacheConfig(cacheNames = "SysRoleMenuInfo",keyGenerator = "myKeyGenerator")
public class SysRoleMenuServiceImpl implements SysRoleMenuService {
	
	
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;
    
    @RequestMapping("/saveOrUpdate")
    @Override
//    @CacheEvict(key = "'SysRoleMenuServiceImpl.queryMenuIdList['+role_id+']'")
    public void saveOrUpdate(@RequestParam("role_id") Long role_id, List<Long> menuIdList) {
        if(menuIdList.size() == 0){
            return ;
        }
        //先删除角色与菜单关系
        sysRoleMenuMapper.delete(role_id);

      //保存角色与菜单关系
        Map<String, Object> map = new HashMap<>();
        map.put("role_id", role_id);
        map.put("menuIdList", menuIdList);
        sysRoleMenuMapper.save(map);
    }

    @RequestMapping("/queryMenuIdList")
    @Override
//    @Cacheable
    public List<Long> queryMenuIdList(@RequestParam("role_id") Long role_id) {
        return sysRoleMenuMapper.queryMenuIdList(role_id);
    }
}
