package cn.lyc.logs;

import cn.lyc.entity.SysLog;
import cn.lyc.entity.SysUser;
import cn.lyc.mq.Producer;
import cn.lyc.util.JDBCUtils;
import cn.lyc.util.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * @ClassName LogsAop
 * @Description aop实现日志切面管理
 * @Author wh
 * #Date 2019-09-04 16:25
 */
@Aspect
@Component
@Slf4j
//@Configuration
public class LogsAop {

//    @Autowired
    private SysLog sysLog = new SysLog();
    @Autowired
    private Producer producer;

    @Pointcut("@annotation(cn.lyc.logs.LogsAnnotation)")
    public void logs() {

    }

    @Around(value = "logs()")
    public void aroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
        pjp.proceed();
        // 获取方法签名
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        // 获取方法
        Method method = methodSignature.getMethod();
        // 获取方法上面的注解
        LogsAnnotation logAnno = method.getAnnotation(LogsAnnotation.class);
        //访问目标方法的参数：
        Object[] args = pjp.getArgs();
        //获取操作详情
        String detail = logAnno.detail();
        //操作时间
        Date date = new Date();
        //获取request对象
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        String authorization = request.getHeader("Authorization");
        String username = "";
        if(authorization != null){
            username =  JWTUtils.getUserName(authorization);
        }else{
            username = request.getParameter("username");
        }
        SysUser sysUser = JDBCUtils.QueryObjectByName(username);
        sysLog.setCreate_time(date);
        sysLog.setUser_id(sysUser.getId());
        sysLog.setMethod_description(detail);
        sysLog.setMethod_name(method.getDeclaringClass().getName()+"."+method.getName());
        sysLog.setRequest_ip(getIpAddress(request));
        producer.sendMsg(sysLog);
    }

    /**
     * 获取Ip地址
     *
     * @param request
     * @return
     */
    private static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
//        if (ip != null) {
//            if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
//                int index = ip.indexOf(",");
//                if (index != -1) {
//                    return ip.substring(0, index);
//                } else {
//                    return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
//                }
//            }
//        }
//        ip = request.getHeader("X-Real-IP");
//        if (ip != null) {
//            if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
//                return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
//            }
//        }
//        ip = request.getHeader("Proxy-Client-IP");
//        if (ip != null) {
//            if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
//                return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
//            }
//        }
//        ip = request.getHeader("WL-Proxy-Client-IP");
//        if (ip != null) {
//            if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
//                return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
//            }
//        }
        ip = request.getRemoteAddr();
        return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
    }
}
