package cn.lyc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import cn.lyc.entity.SysRole;


public interface SysRoleMapper {
	
    /**
     * 根据账户查找其对应的公司或服务商已创建的角色
     * @param map
     * @return
     */
    List<SysRole> queryRolesByUser(Map<String,Object> map);
	
    /**
     * 根据账户查找其对应的公司或服务商已创建的角色数量
     * @param map
     * @return
     */
    int queryRolesByUserCount(Map<String, Object> map);
	
	void save(SysRole role);

	SysRole queryObject(@Param("role_id") Long role_id);
	
    /**
     * 更新一个角色
     * @param sysRole
     * @return
     */
    int updateSysRole(SysRole sysRole);
	
    /**
     * 删除
     * @param role_id
     */
    void deleteSysRole(@Param("role_id") long role_id);
	
	
	
}

