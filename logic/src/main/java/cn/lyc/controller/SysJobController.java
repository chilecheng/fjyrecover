package cn.lyc.controller;

import cn.lyc.api.service.SysJobService;
import cn.lyc.entity.SysJob;
import cn.lyc.utils.R;
import cn.lyc.utils.ShiroUtil;
import cn.lyc.utils.TableReturn;
import cn.lyc.utils.TaskException;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ll moonlight
 * @date: 2019-09-05 9:45
 */
@RestController
@Slf4j
public class SysJobController {

//    @Autowired
//    private jobService jobService;
    @Autowired
    private SysJobService jobService;

    @RequestMapping("sys/job/list")
    public TableReturn getJobList(@RequestBody Map<String, Object> map){
        Object principal = SecurityUtils.getSubject().getPrincipal();

//        subject.hasRole("admin");
//        subject.isPermitted("/user/add");
//        System.out.println(subject.hasRole("admin"));
//        System.out.println(subject.isPermitted("sys/role/list"));
        List<SysJob> jobList = jobService.getJobList(map);
        int total = jobService.queryJobCount();
        return new TableReturn(jobList,total);
    }

    /**
     * 改变定时任务状态
     * @param job
     * @return
     */
    @RequestMapping("sys/job/changeStatus")
    public R changeStatus(@RequestBody SysJob job) throws SchedulerException {
        jobService.changeStatus(job);
        return R.ok();

    }

    /**
     * 运行一次定时任务
     * @param job
     * @return
     */
    @RequestMapping("sys/job/run")
    public R run(@RequestBody SysJob job) throws SchedulerException {
        jobService.run(job);
        return R.ok();
    }

    /**
     * 删除定时任务
     * @param job
     * @return
     */
    @RequestMapping("sys/job/delete")
    public R deleteJob(@RequestBody SysJob job) throws SchedulerException {
        jobService.deleteJob(job);
        return R.ok();
    }

    /**
     * 增加定时任务
     * @param job
     * @return
     */
    @RequestMapping("sys/job/add")
    public R  insertJob(@RequestBody SysJob job) throws TaskException, SchedulerException {
        job.setCreateTime(new Date());
        job.setCreateBy(ShiroUtil.getUserEntity().getUsername());

        jobService.insertJob(job);

        return R.ok();
    }

    /**
     * 更新定时任务
     * @param job
     * @return
     */
    @RequestMapping("sys/job/update")
    public R updateJob(@RequestBody SysJob job) throws TaskException, SchedulerException {
        job.setUpdateTime(new Date());
        job.setUpdateBy(ShiroUtil.getUserEntity().getUsername());
        jobService.updateJob(job);

        return R.ok();
    }

    /**
     * 检查cron
     * @param cronExpression
     * @return
     */
    @RequestMapping("sys/job/checkCRON")
    public R checkCronExpressionIsValid(@RequestParam("cronExpression") String cronExpression){
        boolean b = jobService.checkCronExpressionIsValid(cronExpression);
        if(!b){
            return R.error("cron表达式不对");
        }
        return R.ok();
    }
}
