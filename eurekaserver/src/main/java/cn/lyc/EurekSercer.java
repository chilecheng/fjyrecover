package cn.lyc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})//禁止springboot自动注入数据源配置
@EnableEurekaServer//表示这是一个服务注册中心的服务端
@EnableFeignClients//启动feign客户端
public class EurekSercer {

	public static void main(String[] args) {
		SpringApplication.run(EurekSercer.class, args);
	}

}
