package cn.lyc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

//@Mapper
public interface SysRoleMenuMapper {
	
    /**
     * 保存角色与菜单关系
     * @param map
     */
    void save(Map<String, Object> map);

    /**
     * 根据id删除角色与菜单关系
     * @param id
     * @return
     */
    int delete(@Param("id") long id);

    /**
     * 根据角色ID，获取菜单ID列表
     */
    List<Long> queryMenuIdList(@Param("role_id") Long role_id);
}
