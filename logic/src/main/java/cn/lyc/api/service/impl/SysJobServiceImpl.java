package cn.lyc.api.service.impl;

import cn.lyc.api.service.SysJobService;
import cn.lyc.dao.SysJobMapper;
import cn.lyc.entity.SysJob;
import cn.lyc.utils.ServiceException;
import cn.lyc.utils.TaskException;
import cn.lyc.utils.quartz.CronUtils;
import cn.lyc.utils.quartz.ScheduleConstants;
import cn.lyc.utils.quartz.ScheduleUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ll moonlight
 * @date: 2019-09-04 14:39
 */
@Slf4j
@Service
public class SysJobServiceImpl implements SysJobService {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private SysJobMapper jobMapper;


    /**
     * 项目启动时，初始化定时器
     * 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    public void init() throws SchedulerException, TaskException {
        List<SysJob> jobList = jobMapper.selectJobAll();
        for (SysJob job : jobList) {
            updateSchedulerJob(job, job.getJobGroup());
        }
    }

    /**
     * 更新任务
     *
     * @param job      任务对象
     * @param jobGroup 任务组名
     */
    public void updateSchedulerJob(@RequestBody SysJob job, String jobGroup) throws SchedulerException, TaskException {
        Long jobId = job.getJobId();
        // 判断是否存在
        JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            // 防止创建时存在数据问题 先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey);
        }
        ScheduleUtils.createScheduleJob(scheduler, job);
    }



    @Override
    @RequestMapping("/getJobList")
    public List<SysJob> getJobList(@RequestBody Map<String, Object> map){
        List<SysJob> jobList = jobMapper.jobList(map);
        return jobList;
    }

    /**
     * 通过调度任务ID查询调度信息
     *
     * @param jobId 调度任务ID
     * @return 调度任务对象信息
     */
    @Override
    public SysJob selectJobById(Long jobId) {
        return jobMapper.selectJobById(jobId);
    }

    /**
     * quartz调度器的计划任务列表
     * @param job
     * @return
     */
    @Override
    public List<SysJob> selectAllJob(@RequestBody SysJob job) {
        return jobMapper.selectJobList(job);
    }


    /**
     * 查询定时任务总数
     * @return
     */
    @RequestMapping("/queryJobTotal")
    @Override
    public int queryJobCount() {
        return jobMapper.queryTotal();
    }


    /**
     * 任务调度状态修改
     */
    @Override
    @RequestMapping("/changeStatus")
    public int changeStatus(@RequestBody SysJob job) throws SchedulerException {

        int rows = 0;
        String status = job.getStatus();
        if (ScheduleConstants.Status.NORMAL.getValue().equals(status)) {
            rows = resumeJob(job);
        } else if (ScheduleConstants.Status.PAUSE.getValue().equals(status)) {
            rows = pauseJob(job);
        }
        return rows;
    }


    /**
     * 暂停任务
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int pauseJob(@RequestBody SysJob job) throws SchedulerException {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        int rows = jobMapper.updateJob(job);
        if (rows > 0) {
            scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
        return rows;
    }


    /**
     * 恢复任务
     * @param job 调度信息
     * @return
     * @throws SchedulerException
     */
    @Override
    @Transactional
    public int resumeJob(@RequestBody SysJob job) throws SchedulerException {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.NORMAL.getValue());
        int rows = jobMapper.updateJob(job);
        if (rows > 0) {
            scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
        return rows;
    }


    /**
     * 立即运行任务
     *
     * @param job 调度信息
     * @throws SchedulerException
     */
    @Override
    @Transactional
    @RequestMapping("/runJob")
    public void run(@RequestBody SysJob job) throws SchedulerException {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        SysJob properties = selectJobById(job.getJobId());
        // 参数
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(ScheduleConstants.TASK_PROPERTIES, properties);
        scheduler.triggerJob(ScheduleUtils.getJobKey(jobId, jobGroup), dataMap);
    }

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    @RequestMapping("/removeJob")
    public void deleteJob(@RequestBody SysJob job) throws SchedulerException {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        int rows = jobMapper.deleteJobById(jobId);
        if (rows > 0) {
            scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        } else {
            throw new ServiceException("删除失败");
        }

    }



    /**
     * 新增任务
     *
     * @param job 调度信息 调度信息
     */
    @Override
    @Transactional
    @RequestMapping("/addJob")
    public void insertJob(@RequestBody SysJob job) throws SchedulerException, TaskException {
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        int rows = jobMapper.insertJob(job);
        if (rows > 0) {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
        else {
            throw new ServiceException("添加失败");
        }

    }


    /**
     * 更新任务的时间表达式
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    @RequestMapping("updateJob")
    public void updateJob(@RequestBody SysJob job) throws SchedulerException, TaskException {
        SysJob properties = selectJobById(job.getJobId());
        int rows = jobMapper.updateJob(job);
        if (rows > 0) {
            updateSchedulerJob(job, properties.getJobGroup());
        }else {
            throw new ServiceException("更新失败");
        }

    }





    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    @Override
    @RequestMapping("/checkCronExpressionIsValid")
    public boolean checkCronExpressionIsValid(String cronExpression) {
        return CronUtils.isValid(cronExpression);
    }

}
