package cn.lyc.mq;


import cn.lyc.entity.SysLog;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

/**
 * @ClassName Producer
 * @Description {TODO}
 * @Author wh
 * #Date 2019-08-30 14:14
 */
@Component
public class Producer {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private Queue queue;

    public void sendMsg(SysLog sysLog){
        String s = JSON.toJSONString(sysLog);
        jmsMessagingTemplate.convertAndSend(queue,s);
    }
}
