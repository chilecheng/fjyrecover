package cn.lyc.vo;
import	java.util.Map;

import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: ll moonlight
 * @date: 2019-09-11 14:25
 */
@Data
public class MenuVO {
    private String path;
    private String name;
//    private String menuname;
    private String redirect;
    private String component;
//    private String icon;

    private Map meta;
    private List<MenuVO> children;
}
