package cn.lyc.api.service;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

//@RequestMapping(name = "user")
public interface SysRoleMenuService {
    /**
     * 保存或更新
     * @param role_id
     * @param menuIdList
     */
    void saveOrUpdate(@RequestParam("role_id") Long role_id, @RequestBody List<Long> menuIdList);

    /**
     * 根据角色ID，获取菜单ID列表
     */
    List<Long> queryMenuIdList(@RequestParam("role_id") Long role_id);
}
