package cn.lyc.controller;


import cn.lyc.api.service.SysUserService;
import cn.lyc.entity.SysUser;
import cn.lyc.utils.JWTUtil;
import cn.lyc.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@Slf4j
public class LoginController{

    @Autowired
    private SysUserService userService;

    @Autowired
    private JWTUtil jwtUtil;

	@RequestMapping("login")
	public R login(@RequestParam("username") String username, @RequestParam("password")String password) throws Exception {

	    //用户名加密
        ByteSource credentialsSalt = ByteSource.Util.bytes(username);
        String md5Hex = DigestUtils.md5Hex(credentialsSalt+password);
        //该类会去比较前端密码和数据库密码是否匹配
        SysUser sysusers = userService.query_userName(username);
//        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(sysusers,sysusers.getPassword(),credentialsSalt,credentialsSalt.toBase64());

        if (sysusers.getPassword().equals(md5Hex)) {
//            String token = JWTUtil.sign(username, md5Hex);
            String token = jwtUtil.signRs256(username);
            System.out.println("登录成功=" + token);
            HashMap<String, Object> map = new HashMap<>();
            map.put("token", token);
            return R.ok(map);
        } else {
            System.out.println("登录失败");
            return R.ok("登录失败");
        }
	}


}
