package cn.lyc.controller;


import cn.lyc.api.service.SysMenuService;
import cn.lyc.entity.SysMenu;
import cn.lyc.entity.SysUser;
import cn.lyc.utils.R;
import cn.lyc.utils.RoleUtil;
import cn.lyc.utils.ShiroUtil;
import cn.lyc.utils.TableReturn;
import cn.lyc.vo.MenuVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单功能控制器
 */
@RestController
@Slf4j
public class SysMenuController {
	
    @Autowired
    private SysMenuService menuService;
    /**
     * 用户菜单列表
     */
    @RequestMapping("sys/menu/user")
    public R user(){
        List<SysMenu> menuList = menuService.getUserMenuList(ShiroUtil.getUserEntity().getId());

        return R.ok().put("menuList", menuList);
    }
    /**
     * 用户菜单列表VO
     */
    @RequestMapping("sys/menu/userVo")
    @RequiresAuthentication
    public Object userVO(){
        List<MenuVO> menuList = menuService.getUserMenuVOList(ShiroUtil.getUserEntity().getId());
        return R.ok().put("menuList", menuList);
    }
    
    /**
     * 所有菜单列表
     */
    @RequestMapping("sys/menu/list")
    @RequiresPermissions("sys:menu:list")
    public TableReturn list(@RequestBody Map<String,Object>  map){

        //查询列表数据
        SysUser user = ShiroUtil.getUserEntity();
        int type = RoleUtil.getRoleType(user);
        map.put("is_admin",type);
        List<SysMenu> menuList = menuService.queryList(map);
        int total = menuService.queryTotal();

        return new TableReturn(menuList,total);
    }
    
    /**
     * 角色授权菜单
     */
    @RequestMapping("sys/menu/perms")
    @RequiresPermissions("sys:menu:perms")
    public R perms(){
        //查询列表数据
        SysUser user = ShiroUtil.getUserEntity();
        int type = RoleUtil.getRoleType(user);
//        System.out.println(type);
        Map<String,Object> map = new HashMap<>();
        if(type == 2 || type == 3){
            map.put("user_id",user.getId());
        }
        map.put("is_admin",type);
       List<SysMenu> menuList = menuService.queryList(map);

        return R.ok().put("menuList", menuList);
    }
    
    
    /**
     * 保存
     */
    @RequestMapping("sys/menu/save")
    @RequiresPermissions("sys:menu:save")
    public R save(@RequestBody SysMenu menu){

    	menuService.save(menu);

        return R.ok();
    }
    
    /**
     * 菜单信息
     */
    @RequestMapping("sys/menu/info/{menuId}")
    @RequiresPermissions("sys:menu:info")
    public R info(@PathVariable("menuId") Long menuId){
        SysMenu menu = menuService.queryObject(menuId);
        return R.ok().put("menu", menu);
    }
    
    /**
     * 修改
     */
    @RequestMapping("sys/menu/update")
    @RequiresPermissions("sys:menu:update")
    public R update(@RequestBody SysMenu menu){

    	menuService.update(menu);

        return R.ok();
    }
    
    /**
     * 删除
     */
    @RequestMapping("sys/menu/delete")
    @RequiresPermissions("sys:menu:delete")
    public R delete(@RequestBody Long[] menuIds){
        /*for(Long menuId : menuIds){
            if(menuId.longValue() <= 28){
                return R.error("系统菜单，不能删除");
            }
        }*/
    	menuService.deleteBatch(menuIds);

        return R.ok();
    }

}
