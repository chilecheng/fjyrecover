package cn.lyc.api.service.impl;

import cn.lyc.api.service.SysLogService;
import cn.lyc.dao.SysLogMapper;
import cn.lyc.entity.SysLog;
import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName SysLogServiceImpl
 * @Description {TODO}
 * @Author wh
 * #Date 2019-09-23 11:06
 */
@Slf4j
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper mapper;
    @Override
    public List<SysLog> queryList(Map<String, Object> map) {
        return mapper.queryList(map);
    }

    @Override
    public void save(SysLog sysLog) {
        mapper.save(sysLog);
    }

    @Override
    public int queryListTotal(Map<String, Object> map) {
        return mapper.queryListTotal(map);
    }
}
