package cn.lyc.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sun.misc.BASE64Decoder;

import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.UUID;

@Configuration
public class JWTUtil {

    @Value("${publicKey}")
    private String publicKey;

    @Value("${privateKey}")
    private String privateKey;

//    private  final String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAswqMEJTi6St7XZ0a0VqGW17RVtULWNSy0JDnrT1jo5JiYC5GcEkADcW2hwr6OD0C3hHW1iv0WfU5eSHi31GdOxciAgwrM3sADE9CnG4g79PCS6sJKSwRSBHtkLYp7Gssxg8UvbUvUEe4vuLJPiPjg25laCUc+J3McoQfVcgPR2/4ENTNEzRZscaPc0aJDv0i1AaHShE1dssnqBA5w1T3cw3QsllLQFYwfF5dm5eO+8mBlltzTMWbkNlKrUAwO7kFEpqiKh89DBn3XlN9Ciz4xpicUJfLN+YtEAjzQONUBxVjJWCr+lu4Bzqe3bEfuHN42InUG/GHzC7z0xaIFJfW1QIDAQAB";
//    private  final String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCzCowQlOLpK3tdnRrRWoZbXtFW1QtY1LLQkOetPWOjkmJgLkZwSQANxbaHCvo4PQLeEdbWK/RZ9Tl5IeLfUZ07FyICDCszewAMT0KcbiDv08JLqwkpLBFIEe2QtinsayzGDxS9tS9QR7i+4sk+I+ODbmVoJRz4ncxyhB9VyA9Hb/gQ1M0TNFmxxo9zRokO/SLUBodKETV2yyeoEDnDVPdzDdCyWUtAVjB8Xl2bl477yYGWW3NMxZuQ2UqtQDA7uQUSmqIqHz0MGfdeU30KLPjGmJxQl8s35i0QCPNA41QHFWMlYKv6W7gHOp7dsR+4c3jYidQb8YfMLvPTFogUl9bVAgMBAAECggEAfKpOZslUymilaMSYxMdUoFCAUQbc9Mh9je4n8x+4vk+SQkhWcZ7S88QEWTmSixAoa/pd7hkC9pRipW6r6zWH1npACs7wUdNGcEvMJOIl+vgm5Ms+ALYXfmhFU6sTu/PQZERh+8oqHKsj/34oBCW3KG/5GjAlYRN3JRHMQ82ene0zrVpGikkhscVRkNH8ZlePV+nXA0/9iqDw5kAs7gGoBtQ97nDZ5rp8G3tQsTjZw5ty45w+d5fy7b7ahY3iQqNaz7K8QBN/kzjjYwaQX2QsZDhCWuowctyulxCPoodS1+wwsJs5vRoOZaMAHvtcF6ESS6pPWLCFc2t09W135ylEgQKBgQDxfr9AaM7B9/KyOzuoH7esiKopXiSpLm2XIyowoFShiUE6QjA/8LoJ09J6fthmgsh06uEPeiyGm8rOdR86JKBekPkxikLOdQ2t2CsUDoAMVu74+x50gErGTHxsA5vDxKCX3XXbY74YWN0KwcnEBBXqAqVO+lquJ1VcVcTuL2RwtQKBgQC9y4Cmbdg4rqcG/uEd5AvGxy1zeWhWqLg/L0R6hkca8bDevzNTShCtPeu+a4e4bAyqOzR0nqzh1GQTO10KIEmfe32AsrZE8AIlhQwfUwrL6Edxgr5RI/8jfY7brLMSLIyQ0g1ZDgUDRteqCGrn9xhe3EjdmEwNNJ94ZOKJ4vNBoQKBgQC1hw/eK6pXdbkSLqI+U3EumSBHrkOihIrOVBmPqcVUE6dbUouQVLSTVI1flN+31tE1yaTZjH3RxWPiY3nQq918mD8o9QQYyzY4Gkjvb6LjI6nIsIMJEbKjcrUIQIIi2zfYUBkai9RF2u50+oxe8q9CUzDNuDTXuitZGAPLAwbeDQKBgGI3vOTjtWBn6aHyJpdsWzfL/VZjvr4R4/82u85YTNXBu5EaCzqP+p9civHo/bxPliJGLzgmoTrgycU2FEjYOqFgq6r9UNvOBJ5cAtD3DYBGPSqdyVV+Nlusb1B5R9Yg3cFYInYyjh+K+rJQVtqiAaedgcvDM6Al2lzYgG9w1gfhAoGAI08WyYxPsJWq8feZhd4ub3S/FwsUWX1MTQZiOZEHZToQHgP+qQlNLYXII82KwsxAWjwc4McpxS9bdYsHj9s3NRVo4ZjWMmNRzXoaymAXibGFfLqduLszL+J+PxRNZnivlzbPgJrtgiUslM18LXxGe0o+x2HRbbtWMO59aO0rGeo=";

    @Bean
    public String test(){

        return "";
    }


    /**
     * 生成JWT
     * @param username
     * @return
     * @throws Exception
     */
    public  String signRs256(String username) throws Exception {
        Date now = new Date();

        Date expires = new Date(now.getTime() + 1000*60*60*24*7 /* 7 day */);
        String jwt = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setId(UUID.randomUUID().toString())
                .setSubject(username)
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(expires)
                .signWith(SignatureAlgorithm.RS256, (RSAPrivateKey) getPrivateKey(privateKey))
                .compact();
        return jwt;
    }

    /**
     * 验证JWT
     * @param jwt
     * @return
     */
    public   boolean verify(String jwt){
        try {
            Jwts.parser().setSigningKey((Key) getPublicKey(publicKey)).parseClaimsJws(jwt);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 获取用户名
     * @param jwt
     * @return
     * @throws Exception
     */
    public  String getUserName(String jwt) throws Exception {
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey((Key) getPublicKey(publicKey))
                .parseClaimsJws(jwt);
        String subject = claimsJws.getBody().getSubject();
        return subject;
    }



    /**
     * 将String转化为公钥
     * @param key
     * @return
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
//        keyBytes =   Base64.encodeBase64String(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return (RSAPublicKey)publicKey;
    }


    /**
     * 将String转化为私钥
     * @param key
     * @return
     * @throws Exception
     */
    public  RSAPrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return (RSAPrivateKey)privateKey;
    }


}
