package cn.lyc.api.service.impl;

import cn.lyc.api.service.SysUserRoleService;
import cn.lyc.dao.SysUserRoleMapper;
import cn.lyc.entity.SysRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {
	
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    
    @RequestMapping("/saveUserRole")
    @Override
    public void saveUserRole(@RequestParam("userId") Long userId, List<Long> role_idList) {
        //先删除用户与角色关系
        sysUserRoleMapper.delete(userId);
        if(role_idList.size() != 0){
            //保存用户与角色关系
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            map.put("role_idList", role_idList);
            sysUserRoleMapper.save(map);
        }

    }

    @RequestMapping("/haveRole")
    @Override
    public List<SysRole> haveRole(@RequestParam("id") long id) {
        return sysUserRoleMapper.queryHasRole(id);
    }
}
