package cn.lyc.api.service;




import cn.lyc.entity.SysRole;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 用户角色服务
 */
public interface SysUserRoleService {
    /**
     * 保存或更新
     * @param userId
     * @param role_idList
     */
    void saveUserRole(@RequestParam("userId") Long userId, @RequestBody List<Long> role_idList);

    /**
     * 用户包含角色
     * @param id
     * @return
     */
    List<SysRole> haveRole(@RequestParam("id") long id);
}
