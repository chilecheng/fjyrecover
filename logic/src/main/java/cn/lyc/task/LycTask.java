package cn.lyc.task;

import org.springframework.stereotype.Component;

/**
 * @description: 定时任务默认方法
 * @author: ll moonlight
 * @date: 2019-09-04 15:39
 */
@Component("lycTask")
    public class LycTask {


    public void myTask(String params){
        System.out.println("执行定时任务: "+params);
    }

    public void MultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println("执行有参方法");
        System.out.print(s);
        System.out.println(b);
        System.out.print(l);
        System.out.print(d);
        System.out.print(i);
    }

    public void Params(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void NoParams() {
        System.out.println("执行无参方法");
    }

    public void double1(Double d){
        System.out.println(""+d);
    }

}
