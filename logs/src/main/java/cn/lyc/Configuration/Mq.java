package cn.lyc.Configuration;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.jms.Queue;


/**
 * @ClassName Mq
 * @Description {TODO}
 * @Author wh
 * #Date 2019-08-30 14:19
 */
@Configuration
public class Mq {

    private String queue = "logsQueue";

    @Bean
    public Queue createQueue(){
        return new ActiveMQQueue(queue);
    }
}
