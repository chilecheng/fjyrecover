package cn.lyc.controller;


import cn.lyc.api.service.SysRoleMenuService;
import cn.lyc.api.service.SysRoleService;
import cn.lyc.entity.SysRole;
import cn.lyc.entity.SysUser;
import cn.lyc.utils.R;
import cn.lyc.utils.RoleUtil;
import cn.lyc.utils.ShiroUtil;
import cn.lyc.utils.TableReturn;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 角色控制器
 */
@RestController
@Slf4j
public class SysRoleController {
	

	@Autowired
	private SysRoleService roleService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;
    

    private SysUser sysUser;
    /**
     * 角色列表
     */
    @RequestMapping("sys/role/list")
    @RequiresPermissions("sys:role:list")
    public TableReturn list(@RequestBody Map<String,Object> map){
        sysUser = ShiroUtil.getUserEntity();
        int type = RoleUtil.getRoleType(sysUser);
        map.put("company_id",sysUser.getCompany_id());
        map.put("provider_id",sysUser.getProvider_id());
        map.put("id",sysUser.getId());
        //查询列表数据

        List<SysRole> list = roleService.queryRolesByUser(map);
        int total = roleService.queryRolesByUserCount(map);
        return new TableReturn(list,total);
    }
    
    /**
     * 保存角色
     */
    @RequestMapping("sys/role/save")
    @RequiresPermissions("sys:role:save")
    public R save(@RequestBody SysRole role){
        if(StringUtils.isBlank(role.getRole_name())){
            return R.error("角色名称不能为空");
        }
        sysUser = ShiroUtil.getUserEntity();
        role.setCreate_user(sysUser.getId());
        roleService.save(role);

        return R.ok();
    }
    
    /**
     * 角色信息
     */
    @RequestMapping("sys/role/info/{role_id}")
    @RequiresPermissions("sys:role:info")
    public R info(@PathVariable("role_id") Long role_id){
//        SysRole role = roleService.queryObjectSysRole(role_id);
        SysRole role = roleService.queryObject(role_id);

        //查询角色对应的菜单
//        List<Long> menuIdList = roleService.queryMenuIdList(role_id);
        List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(role_id);
        role.setMenuIdList(menuIdList);

        return R.ok().put("role", role);
    }
    
    /**
     * 修改角色
     */
    @RequestMapping("sys/role/update")
    @RequiresPermissions("sys:role:update")
    public R update(@RequestBody SysRole role){
        if(StringUtils.isBlank(role.getRole_name())){
            return R.error("角色名称不能为空");
        }

        roleService.updateSysRole(role);

        return R.ok();
    }
   
    /**
     * 删除角色
     */
    @RequestMapping("sys/role/deleteSysRole/{role_id}")
    @RequiresPermissions("sys:role:delete")
    public R deleteSysRole(@PathVariable("role_id")long role_id){
    	roleService.deleteSysRole(role_id);
        return R.ok();
    }
    
    
    
}

