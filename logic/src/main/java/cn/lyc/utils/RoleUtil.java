package cn.lyc.utils;

import cn.lyc.entity.SysUser;

/**
 * @ClassName RoleUtil
 * @Description {TODO}
 * @Author wh
 * #Date 2019-06-25 11:14
 */
public class RoleUtil {

    /**
     * 判断用户类型
     * @param user
     * @return int 1表示超级管理员或运营管理员，2表示服务商，3表示企业
     */
    public static int getRoleType(SysUser user){
        Long providerId = user.getProvider_id();
        Long companyId = user.getCompany_id();
        if(providerId == null && companyId == null){
            return 1;
        }else if (companyId == null){
            return 2;
        }else{
            return 3;
        }
    }
}
